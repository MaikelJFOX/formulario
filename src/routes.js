import Vue from "vue";
import VueRouter from 'vue-router'
import Login from './components/Login.vue'
import Registro from './components/Registro.vue'
import Componente from './components/Componente.vue'

Vue.use(VueRouter)

const routes = [
    {path: '/', component: Login},
    {path: '/registro', component: Registro},
    {path: '/componente', component: Componente}

];

const router = new VueRouter({
    mode: "history",
    routes
});

export default router;